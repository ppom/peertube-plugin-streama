# PeerTube plugin Streama

Goal is to make a
[Streama](https://github.com/streamaserver/streama/)-like (e.g. Netflix-like) server
based on [Peertube](https://joinpeertube.org).

This plugin is not ready for production yet.

Feel free to participate by joining the discussion in the issues! If you want to make a PR, please discuss it first to ensure we're on the same page.

## Development

### Subfolder `svelte`

Folder `svelte` is a subproject. It is used to make library components using svelte.
It is automatically installed and built alongside the main npm project using the `preinstall` and `prebuild` commands in [`package.json`](./package.json) (see [npm docs](https://docs.npmjs.com/cli/v9/using-npm/scripts)).

To improve this specific part, you can benefit of its dev server for faster development by running `npm run dev` inside.
