#!/usr/bin/env bash
set -e

npm run build

cd ../PeerTube

node ./dist/server/tools/peertube.js auth add -u 'http://localhost:9000' -U 'root' --password 'test'
node ./dist/server/tools/peertube.js plugins install --path "$OLDPWD"
