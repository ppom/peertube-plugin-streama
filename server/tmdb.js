
async function addTMDBSettings({
	registerSetting,
}) {
	registerSetting({
		name: 'tmdb-api-readkey',
		label: ' TheMovieDB: API Read Access Token',
		type: 'input',
		default: '',
		descriptionHTML: "TheMovieDB's movie/TV show information database is accessible only by registering at themoviedb.org/account/signup and requesting an API Read Access Token. After you have sent a request form, you can see your API Read Access Token by going to the API section in your profile's settings in TheMovieDB. It is required by this application to fetch all the nice Movie/Episode/Show data for you.",
		private: true
	})

	registerSetting({
		name: 'tmdb-api-lang',
		label: ' TheMovieDB: language',
		type: 'input',
		default: 'en-US',
		descriptionHTML: "Language support on TMDb is based on the language query parameter you send along with your API key. For example, you could type fr-FR for getting responses in french. Be careful with your country, fr-FR is not the same as fr-CA. More information at <a href='https://developer.themoviedb.org/docs/languages'>developer.themoviedb.org/docs/languages</a>.",
		private: true
	})

	registerSetting({
		name: 'tmdb-api-adultcontent',
		label: ' TheMovieDB: adult content',
		type: 'input-checkbox',
		default: false,
		descriptionHTML: "Whether to include 🍑 adult content in search results.",
		private: true
	})
}

function returnError(res, msg, code) {
	res.statusCode = code
	res.json({
		error: msg
	})
}

async function addTMDBServerRoute({
	getRouter,
	peertubeHelpers,
	settingsManager
}) {
	const router = getRouter()
	router.get('/search/:type(tv|movie)/:search', async (req, res) => {
		const user = await peertubeHelpers.user.getAuthUser(res)
		if (!user) {
			returnError(res, "You must login to use this route", 401)
			return Promise.resolve()
		}

		if (!req.params.search) {
			returnError(res, "You must provide a parameter: /streama/route/search/exampleSearch", 401)
			return Promise.resolve()
		}

		let [apiKey, apiLang, adultContent] = await Promise.all([
			settingsManager.getSetting('tmdb-api-readkey'),
			settingsManager.getSetting('tmdb-api-lang'),
			settingsManager.getSetting('tmdb-api-adultcontent')
		])

		if (!apiKey) {
			returnError(res, "No API Read Access Token has been set. Check peertube-plugin-streama settings!", 400)
		}
		if (!apiLang) {
			returnError(res, "No API lang has been set. Check peertube-plugin-streama settings!", 400)
		}
		if (typeof adultContent !== "boolean") {
			adultContent = false
		}

		const tmdb = new TMDBClient(apiKey, apiLang, adultContent)

		const response = await tmdb.searchMovie(req.params.type, req.params.search);

		res.json(response)
	})
}

module.exports = {
	addTMDBSettings,
	addTMDBServerRoute
}

class TMDBClient {
	base_url = "https://api.themoviedb.org/"

	constructor(token, lang, adult = false) {
		this.token = token;
		this.lang = lang;
		this.adult = adult;
		this.got_defaults = {
			headers: {
				Authorization: `Bearer ${this.token}`,
			},
			timeout: {
				request: 10 * 1000,
			},
		};
	}

	async searchMovie(type, terms) {
		if (type !== 'tv' && type !== 'movie') {
			return { error: 'Search type must be "tv" or "movie"' }
		}

		const { got } = await import('got')
		const response = await got({
			url: `${this.base_url}/3/search/${type}`,
			searchParams: {
				query: terms,
				language: this.lang,
				include_adult: this.adult,
			},
		}, this.got_defaults).json()
		return response
	}

}

