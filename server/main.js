const { addTMDBSettings, addTMDBServerRoute } = require('./tmdb')

async function register(options) {
	options.peertubeHelpers.logger.info('Registering plugin Streama')

	changeDefaults(options)

	addTMDBSettings(options)
	addTMDBServerRoute(options)

	return Promise.resolve()
}

async function unregister() { }

module.exports = { register, unregister }

async function changeDefaults({
	playlistPrivacyManager,
	videoPrivacyManager,
}) {
	videoPrivacyManager.deleteConstant(1) // PUBLIC
	videoPrivacyManager.deleteConstant(2) // UNLISTED

	playlistPrivacyManager.deleteConstant(1) // PUBLIC
	playlistPrivacyManager.deleteConstant(2) // UNLISTED
}
