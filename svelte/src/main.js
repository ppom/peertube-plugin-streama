import TMDBSearch from './TMDBSearch.svelte'

const app = new TMDBSearch({
	target: document.getElementById('tmdbsearch'),
})

export default app;
