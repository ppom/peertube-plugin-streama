import { resolve } from 'path'
import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [svelte()],
	build: {
		lib: {
			entry: resolve(__dirname, "src", "TMDBSearch.svelte"),
			name: 'TMDBSearch',
			fileName: 'tmdbsearch',
		},
		minify: false,
		rollupOptions: {},
		manifest: true,
	},
})
