import TMDBSearch from '../svelte/dist/tmdbsearch.js'

function register(options) {
	addSearch(options)
}

export {
	register
}

function addSearch({
	peertubeHelpers
}) {
	const anchor = document.createElement("div")
	const parent = document.getElementById('name').parentElement.parentElement
	parent.insertBefore(anchor, parent.childNodes[0])

	new TMDBSearch({
		target: anchor,
		props: {
			peertubeHelpers: peertubeHelpers,
		},
	})
}
