const path = require('path')
const esbuild = require('esbuild')

const clientFiles = [
	'common.js',
	'video-edit.js'
]

const configs = clientFiles.map(f => ({
	entryPoints: [path.resolve(__dirname, '..', 'client', f)],
	bundle: true,
	minify: false,
	format: 'esm',
	target: 'safari11',
	outfile: path.resolve(__dirname, '..', 'dist', f),
}))

const promises = configs.map(c => esbuild.build(c))

Promise.all(promises)
	.catch(() => process.exit(1))
